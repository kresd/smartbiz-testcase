import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  constructor(public dialog: MatDialog, private auth: AuthenticationService) { }

  ngOnInit(): void {
  }

  openDialogLogout() {
    this.dialog.open(LogoutDialog, {
      width: '40%',
    });
  }
}

@Component({
  selector: 'logout-dialog',
  templateUrl: '../login/logout-dialog.html',
})
export class LogoutDialog {
  constructor(
    public dialogRef: MatDialogRef<LogoutDialog>,
    private auth: AuthenticationService) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  logout(): void {
    this.auth.logout();
    this.dialogRef.close();
  }
}