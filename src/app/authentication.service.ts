import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  baseUrl = 'https://api.smartbiz.id/api/login'
  constructor(private http: HttpClient, private router: Router) { }
  setLoggedIn(value: boolean) {
    this.loggedInStatus = value
    localStorage.setItem('loggedIn', 'true')
  }

  setToken(value) {
    localStorage.setItem('token', value);
  }

  get isLoggedIn() {
    return JSON.parse(localStorage.getItem('loggedIn') || this.loggedInStatus.toString())
  }

  loginUser(postLogin) {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization',
      'Bearer ' + localStorage.getItem('loggedIn'));
    return this.http.post<any>(this.baseUrl, postLogin, { headers: headers })
  }

  logout() {
    localStorage.removeItem('loggedIn');
    this.router.navigate(['']);
    window.location.reload();
  }
}