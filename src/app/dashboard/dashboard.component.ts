import { formatDate } from '@angular/common';
import { Component, ViewChild, OnInit, AfterViewInit, Inject } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee/employee.service';
import { EmployeeModelDTO } from '../models/employee.model';

export interface DialogData {
  id: string;
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['nip', 'full_name', 'nick_name', 'birth_date', 'address', 'phone', 'mobile', 'email', 'id'];
  dataSource: MatTableDataSource<EmployeeModelDTO>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public dialog: MatDialog, private _service: EmployeeService, private _router: Router
  ) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
  }

  ngAfterViewInit() {

    this._service.getAllEmployee(EmployeeModelDTO).subscribe(
      resp => {
        this._service.datatable(EmployeeModelDTO, resp.meta.total).subscribe(
          resp2 => {

            let datasrc = Array();
            for (let index = 0; index < resp2.rows.length; index++) {
              let split = resp2.rows[index].birth_date.split(" ", 2);
              const birth_date = split[0];
              let email;
              if (resp2.rows[index].email == null) {
                email = '-';
              } else {
                email = resp2.rows[index].email;
              }
              datasrc.push({
                id: resp2.rows[index].id,
                nip: resp2.rows[index].nip,
                full_name: resp2.rows[index].full_name,
                nick_name: resp2.rows[index].nick_name,
                birth_date: birth_date,
                address: resp2.rows[index].address,
                phone: resp2.rows[index].phone,
                mobile: resp2.rows[index].mobile,
                email: email
              })
            }
            // console.log(datasrc);
            this.dataSource = new MatTableDataSource(datasrc);
            this.dataSource.sort = this.sort;
            this.dataSource.paginator = this.paginator;
          }
        );
      }
    );
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openDialogEdit(id) {
    this._router.navigate(['admin', 'edit-employee', 'edit', id]);
  }

  openDialogDelete(id) {
    this.dialog.open(DeleteEmployeeDialog, {
      data: { id: id }
    });
  }
}

@Component({
  templateUrl: './delete-employee-dialog.component.html',
})

export class DeleteEmployeeDialog {

  constructor(
    public dialogRef: MatDialogRef<DeleteEmployeeDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private employeeService: EmployeeService,
    private _snackBar: MatSnackBar,
    private _router: Router
  ) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  deleteEmployee(id) {
    this.employeeService.delete(id).subscribe(
      data => {
        if (data.message == "Employee was deleted") {
          this.openSnackBar('Employee Has Been Deleted', 'Success');
          this.dialogRef.close();
          this._router.navigate(['admin', 'dashboard']).then(() => {
            window.location.reload();
          });
        }
        else {
          this.openSnackBar('Can\'t Delete Data', 'Error');
        }
      }
    );
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'top'
    });
  }
}