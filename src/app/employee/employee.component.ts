import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { EmployeeService } from './employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
})
export class EmployeeComponent implements OnInit {

  addEmployeeForm: FormGroup;

  constructor(private _snackBar: MatSnackBar, private fb: FormBuilder, private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
    this.addEmployeeForm = this.fb.group({
      nip: new FormControl('', [Validators.required]),
      full_name: new FormControl('', [Validators.required]),
      nick_name: new FormControl('', [Validators.required]),
      birth_date: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      mobile: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required])
    })
  }

  onSubmit() {
    let patchValue = ({
      nip: this.addEmployeeForm.value.nip,
      full_name: this.addEmployeeForm.value.full_name,
      nick_name: this.addEmployeeForm.value.nick_name,
      birth_date: formatDate(this.addEmployeeForm.value.birth_date, 'yyyy-mm-dd', 'en-US'),
      address: this.addEmployeeForm.value.address,
      phone: this.addEmployeeForm.value.phone,
      mobile: this.addEmployeeForm.value.mobile,
      email: this.addEmployeeForm.value.email
    });

    this.employeeService.create(patchValue)
      .subscribe(data => {
        if (data.message == "Employee was created") {
          this.router.navigate(['/admin/employee']);
          this.openSnackBar('New Employee Has Been Saved', 'Sucess');
          this.router.navigate(['admin', 'dashboard']);
        }
        else {
          this.openSnackBar('Can\'t Save Employee', 'Error');
        }
      });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'top'
    });
  }
}
