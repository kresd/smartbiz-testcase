import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EmployeeModelDTO } from '../models/employee.model';
import { DatatablesModelResponse } from '../models/utils/datatables.model';

const baseUrl = 'https://api.smartbiz.id/api/demo/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) { }

  getHeaders() {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization',
      'Bearer ' + localStorage.getItem('token'));

    return headers;
  }

  datatable(value, length): Observable<any> {

    let data = ({
      'query': value,
      "pagination": {
        "page": 1,
        "perpage": length
      },
      "sort": {
        "sort": "ASC",
        "field": "id"
      }
    });

    return this.http.post(`${baseUrl}/page-search`, data, { headers: this.getHeaders() });
  }

  getAllEmployee(value): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.set('Authorization',
      'Bearer ' + localStorage.getItem('token'));

    let data = ({
      'query': value,
      "pagination": {
        "page": 1,
        "perpage": 1
      },
      "sort": {
        "sort": "ASC",
        "field": "id"
      }
    });

    return this.http.post(`${baseUrl}/page-search`, data, { headers: this.getHeaders() });
  }

  get(id): Observable<any> {

    return this.http.get(`${baseUrl}/${id}`, { headers: this.getHeaders() });
  }

  create(data): Observable<any> {
    return this.http.post(baseUrl, data, { headers: this.getHeaders() });
  }

  update(id, data): Observable<any> {
    return this.http.put(`${baseUrl}/${id}`, data, { headers: this.getHeaders() });
  }

  delete(id): Observable<any> {
    return this.http.delete(`${baseUrl}/${id}`, { headers: this.getHeaders() });
  }
}