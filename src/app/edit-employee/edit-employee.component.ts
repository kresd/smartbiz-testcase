import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { EmployeeService } from '../employee/employee.service';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.scss']
})
export class EditEmployeeComponent implements OnInit {

  editEmployeeForm: FormGroup;
  private subcribeTopic: Subscription;

  constructor(private _snackBar: MatSnackBar, private fb: FormBuilder, private employeeService: EmployeeService, private _activeRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {

    this.editEmployeeForm = this.fb.group({
      id: new FormControl('', [Validators.required]),
      nip: new FormControl('', [Validators.required]),
      full_name: new FormControl('', [Validators.required]),
      nick_name: new FormControl('', [Validators.required]),
      birth_date: new FormControl('', [Validators.required]),
      address: new FormControl('', [Validators.required]),
      phone: new FormControl('', [Validators.required]),
      mobile: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required])
    });

    this.subcribeTopic = this._activeRoute.params.subscribe((params: Params) => {
      const id = params['id'];
      this.employeeService.get(id)
        .subscribe(resp => {
          let split = resp.birth_date.split(" ", 2);
          let date = new Date(split[0]);
          this.openSnackBar('Edit Data Employee Ready', 'Go Edit');

          this.editEmployeeForm.setValue({
            id: resp.id,
            nip: resp.nip,
            full_name: resp.full_name,
            nick_name: resp.nick_name,
            birth_date: date,
            address: resp.address,
            phone: resp.phone,
            mobile: resp.mobile,
            email: resp.email
          });
        });
    }, error => {
      this.openSnackBar('Can\'t Get Data Of Employee', 'Error');
    });

  }

  onSubmit() {
    const value = this.editEmployeeForm.value;

    let patchValue = ({
      nip: value.nip,
      full_name: value.full_name,
      nick_name: value.nick_name,
      birth_date: formatDate(value.birth_date, 'yyyy-mm-dd', 'en-US'),
      address: value.address,
      phone: value.phone,
      mobile: value.mobile,
      email: value.email
    });

    this.employeeService.update(value.id, patchValue)
      .subscribe(data => {
        if (data.message == "Employee was updated") {
          this.openSnackBar('New Employee Has Been Updated', 'Success');
          this._router.navigate(['admin', 'dashboard']);
        }
        else {
          this.openSnackBar('Can\'t Update Employee', 'Error');
        }
      });
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
      verticalPosition: 'top'
    });
  }

}
