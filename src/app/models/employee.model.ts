export class EmployeeModelDTO {
    public id: string;
    public nip: string;
    public full_name: string;
    public nick_name: string;
    public birth_date: string;
    public address: string;
    public phone: string;
    public mobile: string;
    public email: string;
}