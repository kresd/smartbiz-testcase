import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  data: any = {}
  hide = true;

  constructor(private fb: FormBuilder, private authenticationService: AuthenticationService, private router: Router) { }


  ngOnInit(): void {
    this.loginForm = this.fb.group({
      identity: [''],
      password: ['']
    })
  }

  onSubmit() {
    this.authenticationService.loginUser(this.loginForm.value)
      .subscribe(data => {
        if (data.message == "Logged") {
          this.router.navigate(['/admin/dashboard']);
          this.authenticationService.setLoggedIn(true);
          this.authenticationService.setToken(data.data.token.access_token);
        }
        else {
          console.log('Tidak Dapat Login');
        }
      });
  }

  logout() {
    this.authenticationService.logout();
  }
}